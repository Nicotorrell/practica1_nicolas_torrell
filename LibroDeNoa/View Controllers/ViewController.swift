//
//  ViewController.swift
//  LibroDeNoa
//
//  Created by Enric Vergara Carreras on 5/12/17.
//  Copyright © 2017 dedam. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ViewController: UIViewController {

    var troncClicked = false
    
    //---IBOutlets:
    @IBOutlet weak var carlota_cos: UIImageView!
    @IBOutlet weak var carlota_cap: UIImageView!
    @IBOutlet weak var img_TietCos: UIImageView!
    @IBOutlet weak var img_TietCap: UIImageView!
    @IBOutlet weak var img_TietaCos: UIImageView!
    @IBOutlet weak var tieta_cap: UIImageView!
    @IBOutlet weak var texto_inf: UILabel!
    @IBOutlet weak var carlota_view: UIView!
    @IBOutlet weak var text_baarada_tiet: UILabel!
    @IBOutlet weak var bafarada_tiet: UIImageView!
    @IBOutlet weak var img_pajaro: UIImageView!
    @IBOutlet weak var sargantana: UIImageView!
    @IBOutlet weak var sargantana_cua: UIImageView!
    @IBOutlet weak var text_bafanada_tieta: UILabel!
    @IBOutlet weak var bafanada_tieta: UIImageView!
    @IBOutlet weak var text_bufanada_carlota: UILabel!
    @IBOutlet weak var bafanada_carlota: UIImageView!
    @IBOutlet weak var bafanada_cat: UIImageView!
    @IBOutlet weak var text_bafanada_cat: UILabel!
    @IBOutlet weak var gato: UIImageView!
    //------------
    
    var sonidos: AVAudioPlayer?
    
    //---IBActions:
    
    @IBAction func button_bafanada_cat(_ sender: Any) {
        
        bafanada_cat.isHidden = false
        text_bafanada_cat.isHidden = false
        text_bafanada_cat.text = NSLocalizedString("bafarada_cat", comment: "")
        let tiet1 = UIImage(named: "01_merli_estatic.png")
        let tiet2 = UIImage(named: "01_merli_parlant1.png")
        gato.animationImages = [tiet1!, tiet2!]
        gato.animationRepeatCount = 0
        gato.animationDuration = 2.0
        gato.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            // Put your code which should be executed with a delay here
            self.gato.stopAnimating()
            self.bafanada_cat.isHidden = true
            self.text_bafanada_cat.isHidden = true
        })
    }
    
    
    @IBAction func buttonbafanada_carlota(_ sender: Any) {
        
        bafanada_carlota.isHidden = false
        text_bufanada_carlota.isHidden = false
        text_bufanada_carlota.text = NSLocalizedString("bafarada_carlota", comment: "")
        let tiet1 = UIImage(named: "01_Carlota_xerrant_01.png")
        let tiet2 = UIImage(named: "01_Carlota_xerrant_02.png")
        carlota_cap.animationImages = [tiet1!, tiet2!]
        carlota_cap.animationRepeatCount = 0
        carlota_cap.animationDuration = 2.0
        carlota_cap.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            // Put your code which should be executed with a delay here
            self.carlota_cap.stopAnimating()
            self.bafanada_carlota.isHidden = true
            self.text_bufanada_carlota.isHidden = true
        })
        
    }
    @IBAction func button_bafanada_tieta(_ sender: Any) {
        
        bafanada_tieta.isHidden = false
        text_bafanada_tieta.isHidden = false
        text_bafanada_tieta.text = NSLocalizedString("bafarada_tieta", comment: "")
        let tiet1 = UIImage(named: "01_tieta_cap_parlantcanto_01.png")
        let tiet2 = UIImage(named: "01_tieta_cap_parlantcanto_02.png")
        tieta_cap.animationImages = [tiet1!, tiet2!]
        tieta_cap.animationRepeatCount = 0
        tieta_cap.animationDuration = 2.0
        tieta_cap.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            // Put your code which should be executed with a delay here
            self.tieta_cap.stopAnimating()
            self.bafanada_tieta.isHidden = true
            self.text_bafanada_tieta.isHidden = true
        })
        
    }
    @IBAction func sargantana_button(_ sender: Any) {
        //y 453
        sargantana_cua.isHidden = false
        let sargantana1 = UIImage(named: "01_Sargantana_escuada1.png")
        let sargantana2 = UIImage(named: "01_Sargantana_escuada2.png")
        
        UIView.animate(withDuration: 5, animations: {
            self.sargantana.frame.origin.y = -self.view.frame.size.height - self.sargantana.frame.size.height
            self.sargantana.animationImages = [sargantana1!, sargantana2!]
            self.sargantana.animationRepeatCount = 0
            self.sargantana.animationDuration = 2.0
            self.sargantana.startAnimating()
        }, completion: nil)
        
        UIView.animate(withDuration: 1, animations: {
            self.sargantana_cua.frame.origin.y = 435
        }, completion: nil)
        
        
        
    }
    @IBAction func button_pajaro(_ sender: Any) {
        let pajaroIN = UIImage(named: "01_ocell_empren_vol.png")
        let pajaro1 = UIImage(named: "01_ocell_vola1.png")
        let pajaro2 = UIImage(named: "01_ocell_vola2.png")
        img_pajaro.animationImages = [pajaroIN!]
        img_pajaro.animationRepeatCount = 0
        img_pajaro.animationDuration = 2.0
        img_pajaro.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150), execute: {
            UIView.animate(withDuration: 4, animations: {
                self.img_pajaro.frame.origin.x = self.view.frame.size.width
                    + self.img_pajaro.frame.size.width
                self.img_pajaro.animationImages = [pajaro1!, pajaro2!]
                self.img_pajaro.animationRepeatCount = 0
                self.img_pajaro.animationDuration = 1.5
                self.img_pajaro.startAnimating()
            }, completion: nil)
            
        })
    }
    @IBAction func button_bafarada(_ sender: Any) {
        bafarada_tiet.isHidden = false
        text_baarada_tiet.isHidden = false
        text_baarada_tiet.text = NSLocalizedString("bafarada_tiet", comment: "")
        let tiet1 = UIImage(named: "01_tiet_CAP_estatic.png")
        let tiet2 = UIImage(named: "01_tiet_CAP_parlant1.png")
        img_TietCap.animationImages = [tiet1!, tiet2!]
        img_TietCap.animationRepeatCount = 0
        img_TietCap.animationDuration = 2.0
        img_TietCap.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            // Put your code which should be executed with a delay here
            self.img_TietCap.stopAnimating()
            self.bafarada_tiet.isHidden = true
            self.text_baarada_tiet.isHidden = true
        })
    
    }
    @IBAction func tietCos_Pressed(_ sender: Any) {
        if(!troncClicked) {
            img_TietCos.stopAnimating()
            config_TietTronc()
            img_TietCos.startAnimating()
            playsound(fileName: "copfusta_tiet2",_extension:"mp3")
            //TODO: play sound "copfusta_tiet2.mp3"
        }else{
            config_TietNormal()
            img_TietCos.startAnimating()            
        }
    }
    //-------------
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        config_TietNormal()
        img_TietCos.startAnimating()
        config_Tieta()
        img_TietaCos.startAnimating()
        config_Carlota()
        
        texto_inf.text = NSLocalizedString("texto", comment: "")
        //var musica:String = NSLocalizedString("texto_voz", comment: "")
        playsound(fileName: NSLocalizedString("texto_voz", comment: ""), _extension: ".mp3")
        //label.text = NSLocalizedString("key", comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func config_TietNormal(){
        troncClicked = false;
        
        let tiet1 = UIImage(named: "01_tiet_copet_destral1.png")
        let tiet2 = UIImage(named: "01_tiet_estatic.png")
        img_TietCos.animationImages = [tiet1!, tiet2!]
        img_TietCos.animationRepeatCount = 0
        img_TietCos.animationDuration = 2.0
    }
    
    private func config_Tieta(){
        let tieta1 = UIImage(named: "01_tieta_Regant_01.png")
        let tieta2 = UIImage(named: "01_tieta_Regant_02.png")
        img_TietaCos.animationImages = [tieta1!, tieta2!]
        img_TietaCos.animationRepeatCount = 0
        img_TietaCos.animationDuration = 2.0
    }
    
    private func config_TietTronc(){
        troncClicked = true;
        
        let tiet1 = UIImage(named: "01_tiet_Partint_tronc1.png")
        let tiet2 = UIImage(named: "01_tiet_Partint_tronc2.png")
        let tiet3 = UIImage(named: "01_tiet_Partint_tronc3.png")
        img_TietCos.animationImages = [tiet1!, tiet2!, tiet3!]
        img_TietCos.animationRepeatCount = 1
        img_TietCos.animationDuration = 1.5
        
    }
    
    
    private func config_Carlota(){
        //carlota_cos.frame.origin.x = -view.frame.size.width x373 y290
        carlota_view.frame.origin.x = -view.frame.size.width
        let carlota1 = UIImage(named:"01_Carlota_bici01.png")
        let carlota2 = UIImage(named:"01_Carlota_bici02.png")
        let carlota3 = UIImage(named:"01_Carlota_bici03.png")
        let carlota4 = UIImage(named:"01_Carlota_bici04.png")
        let carlota5 = UIImage(named:"01_Carlota_bici05.png")
        let carlota6 = UIImage(named:"01_Carlota_bici06.png")
        let carlota7 = UIImage(named:"01_Carlota_bici07.png")
        let carlota8 = UIImage(named:"01_Carlota_bici08.png")
        let carlota9 = UIImage(named:"01_Carlota_bici09.png")
        let carlota10 = UIImage(named:"01_Carlota_bici10.png")
        let carlota11 = UIImage(named:"01_Carlota_bici11.png")
        let carlota12 = UIImage(named:"01_Carlota_bici12.png")
        let carlota13 = UIImage(named:"01_Carlota_bici13.png")
        let carlota14 = UIImage(named:"01_Carlota_bici14.png")
        carlota_cos.animationImages = [carlota1!, carlota2!, carlota3! , carlota4!, carlota5!, carlota6!, carlota7!, carlota8!, carlota9!, carlota10!, carlota11!, carlota12!, carlota13!, carlota14!]
        carlota_cos.animationRepeatCount = 0
        carlota_cos.animationDuration = 1
        carlota_cos.animationRepeatCount = 4
        carlota_cos.startAnimating()        
        UIView.animate(withDuration: 4, animations: {
            self.carlota_view.frame.origin.x = 373
        }, completion: nil)
        
    }
    
    private func playsound(fileName:String, _extension:String){
        let path = Bundle.main.path(forResource: fileName, ofType:_extension)!
        let url = URL(fileURLWithPath: path)
        do {
            sonidos = try AVAudioPlayer(contentsOf: url)
            sonidos?.play()
        } catch {
            // couldn't load file :(
            print("couldn't load file :(")
        }
    }


}

